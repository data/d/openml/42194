# OpenML dataset: Football_players_Fifa_stats

https://www.openml.org/d/42194

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The dataset contains all the statistics for each player from 2008 to 2016.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42194) of an [OpenML dataset](https://www.openml.org/d/42194). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42194/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42194/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42194/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

